import os
import sys
from modules import ExcelConcat
from modules import ML
from modules import MongoDB
from modules import MySQLDB
from modules import SQLiteDB


if __name__ == "__main__":
    os.chdir("CAR")  # folder with data

    ec = ExcelConcat.ExcelConcat()
    ec.main()

    choice = input('''
If you want to use machine learning module press 1;
If you want to use MongoDB module press 2;
If you want to use MySQL module press 3;
If you want to use SQLite module press 4;
If you want to quit the application press 5;
Your choice: ''')

    if choice is '1':
        ml = ML.ML()
        ml.main()
    elif choice is '2':
        mongo = MongoDB.MongoDB()
        mongo.main()
    elif choice is '3':
        mysql = MySQLDB.MySQLDB()
        mysql.main()
    elif choice is '4':
        sqlite = SQLiteDB.SQLiteDB()
        sqlite.main()
    elif choice is '5':
        sys.exit()
    else:
        print("Input is incorrect")
        sys.exit(1)
