# excelCommonTable

## How to use

1.  Download the project
2.	Launch main.py file
3.	Choose the option you want to use within the data

## Modules

1.	ExcelConcat module concatenates data from all files in CAR folder
2.	ML module provides functionality to analyze data and predict values based on the data
3.	MongoDB module creates MongoDB database with data from files
4.	MySQLDB module creates MySQL database with data from files
5.	SQLiteDB module creates an SQLite database with the data from files

## Built with the use

[OpenPyXL library](https://openpyxl.readthedocs.io/en/stable/)<br>
[PyMongo library](https://api.mongodb.com/)<br>
[SQLite library](https://sqlite.org/index.html)<br>
[PyMySQL library](https://pymysql.readthedocs.io/en/latest/)<br>
[pandas library](https://pandas.pydata.org/docs/index.html)<br>