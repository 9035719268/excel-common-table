import openpyxl


class ExcelConcat():
    """Concats data from multiple excel files"""

    def header(self, outputsheet, inputsheet):
        """Parses header into output file"""
        outputsheet['A1'].value = inputsheet['A1'].value
        outputsheet['B1'].value = inputsheet['A2'].value
        outputsheet['C1'].value = inputsheet['A3'].value
        outputsheet['D1'].value = inputsheet['A4'].value
        outputsheet['E1'].value = inputsheet['A5'].value
        outputsheet['F1'].value = inputsheet['A6'].value
        outputsheet['G1'].value = inputsheet['A7'].value
        outputsheet['H1'].value = inputsheet['A8'].value
        outputsheet['I1'].value = inputsheet['A9'].value
        outputsheet['J1'].value = inputsheet['A10'].value
        outputsheet['K1'].value = inputsheet['A11'].value
        outputsheet['L1'].value = inputsheet['A12'].value
        outputsheet['M1'].value = inputsheet['A13'].value
        outputsheet['N1'].value = inputsheet['A14'].value
        outputsheet['O1'].value = inputsheet['A15'].value
        outputsheet['P1'].value = inputsheet['A16'].value
        outputsheet['Q1'].value = inputsheet['A17'].value
        outputsheet['R1'].value = inputsheet['A18'].value
        outputsheet['S1'].value = inputsheet['A19'].value
        outputsheet['T1'].value = inputsheet['A20'].value
        outputsheet['U1'].value = inputsheet['A21'].value
        outputsheet['V1'].value = inputsheet['A22'].value
        outputsheet['W1'].value = inputsheet['A23'].value
        outputsheet['X1'].value = inputsheet['A24'].value
        outputsheet['Y1'].value = inputsheet['A25'].value
        outputsheet['Z1'].value = inputsheet['A26'].value
        outputsheet['AA1'].value = inputsheet['A27'].value
        outputsheet['AB1'].value = inputsheet['A28'].value
        outputsheet['AC1'].value = inputsheet['A29'].value
        outputsheet['AD1'].value = inputsheet['A30'].value
        outputsheet['AE1'].value = inputsheet['A31'].value
        outputsheet['AF1'].value = inputsheet['A32'].value
        outputsheet['AG1'].value = inputsheet['A33'].value
        outputsheet['AH1'].value = inputsheet['A34'].value
        outputsheet['AI1'].value = inputsheet['A35'].value
        outputsheet['AJ1'].value = inputsheet['A36'].value
        outputsheet['AK1'].value = inputsheet['A37'].value
        outputsheet['AL1'].value = inputsheet['A38'].value
        outputsheet['AM1'].value = inputsheet['A39'].value
        outputsheet['AN1'].value = inputsheet['A40'].value
        outputsheet['AO1'].value = inputsheet['A41'].value
        outputsheet['AP1'].value = inputsheet['A42'].value

    def parser(self, outputsheet, inputsheet, num):
        """Parses values into output file"""
        outputsheet['A' + str(num + 1)].value = inputsheet['B1'].value
        outputsheet['B' + str(num + 1)].value = inputsheet['B2'].value
        outputsheet['C' + str(num + 1)].value = inputsheet['B3'].value
        outputsheet['D' + str(num + 1)].value = inputsheet['B4'].value
        outputsheet['E' + str(num + 1)].value = inputsheet['B5'].value
        outputsheet['F' + str(num + 1)].value = inputsheet['B6'].value
        outputsheet['G' + str(num + 1)].value = inputsheet['B7'].value
        outputsheet['H' + str(num + 1)].value = inputsheet['B8'].value
        outputsheet['I' + str(num + 1)].value = inputsheet['B9'].value
        outputsheet['J' + str(num + 1)].value = inputsheet['B10'].value
        outputsheet['K' + str(num + 1)].value = inputsheet['B11'].value
        outputsheet['L' + str(num + 1)].value = inputsheet['B12'].value
        outputsheet['M' + str(num + 1)].value = inputsheet['B13'].value
        outputsheet['N' + str(num + 1)].value = inputsheet['B14'].value
        outputsheet['O' + str(num + 1)].value = inputsheet['B15'].value
        outputsheet['P' + str(num + 1)].value = inputsheet['B16'].value
        outputsheet['Q' + str(num + 1)].value = inputsheet['B17'].value
        outputsheet['R' + str(num + 1)].value = inputsheet['B18'].value
        outputsheet['S' + str(num + 1)].value = inputsheet['B19'].value
        outputsheet['T' + str(num + 1)].value = inputsheet['B20'].value
        outputsheet['U' + str(num + 1)].value = inputsheet['B21'].value
        outputsheet['V' + str(num + 1)].value = inputsheet['B22'].value
        outputsheet['W' + str(num + 1)].value = inputsheet['B23'].value
        outputsheet['X' + str(num + 1)].value = inputsheet['B24'].value
        outputsheet['Y' + str(num + 1)].value = inputsheet['B25'].value
        outputsheet['Z' + str(num + 1)].value = inputsheet['B26'].value
        outputsheet['AA' + str(num + 1)].value = inputsheet['B27'].value
        outputsheet['AB' + str(num + 1)].value = inputsheet['B28'].value
        outputsheet['AC' + str(num + 1)].value = inputsheet['B29'].value
        outputsheet['AD' + str(num + 1)].value = inputsheet['B30'].value
        outputsheet['AE' + str(num + 1)].value = inputsheet['B31'].value
        outputsheet['AF' + str(num + 1)].value = inputsheet['B32'].value
        outputsheet['AG' + str(num + 1)].value = inputsheet['B33'].value
        outputsheet['AH' + str(num + 1)].value = inputsheet['B34'].value
        outputsheet['AI' + str(num + 1)].value = inputsheet['B35'].value
        outputsheet['AJ' + str(num + 1)].value = inputsheet['B36'].value
        outputsheet['AK' + str(num + 1)].value = inputsheet['B37'].value
        outputsheet['AL' + str(num + 1)].value = inputsheet['B38'].value
        outputsheet['AM' + str(num + 1)].value = inputsheet['B39'].value
        outputsheet['AN' + str(num + 1)].value = inputsheet['B40'].value
        outputsheet['AO' + str(num + 1)].value = inputsheet['B41'].value
        outputsheet['AP' + str(num + 1)].value = inputsheet['B42'].value

    def main(self):
        """Program entry point"""
        numFiles = 10149  # number of Excel files to concatenate

        wbTotal = openpyxl.Workbook()  # output file workbook
        sheetTotal = wbTotal.active  # output file worksheet

        # Data parsing process.
        for i in range(1, numFiles + 1):
            wbTemp = openpyxl.load_workbook(str(i) + ".xlsx")  # open all files with .xlsx extension in dir
            sheetTemp = wbTemp.active  # specify worksheet in workbook
            ExcelConcat.header(self, sheetTotal, sheetTemp)  # write header
            ExcelConcat.parser(self, sheetTotal, sheetTemp, i)  # write data
            print("File no.", i, "of", numFiles, "handled")  # notifier
            wbTemp.close()  # close temp workbook

        wbTotal.save("TotalWB.xlsx")  # save workbook with total data
        print("Data was successfully written")  # notifier