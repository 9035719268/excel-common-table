from pymongo import MongoClient
import openpyxl


class MongoDB:
    """Working with MongoDB database"""

    def main(self):
        """Program entry point"""

        password = input("Input your database password: ")
        dbname = input("Input your database name: ")
        colname = input("Input your collection name: ")

        # Connect to database.
        cluster = MongoClient(
            "mongodb+srv://dbuser:" + password + "@cluster0-dzh2r.mongodb.net/test?retryWrites=true&w=majority")
        db = cluster[dbname]  # database name
        collection = db[colname]  # collection name

        workbook = openpyxl.load_workbook("TotalWB.xlsx")  # output file workbook
        sheet = workbook.active  # specify workbook sheet

        # Transfer data to database.
        for rows in range(2, 10151):
            for row in sheet.iter_rows(min_row=rows, max_row=rows, max_col=42, values_only=True):
                post = {
                    '_id': str(rows - 1),
                    'client': str(row[0]),
                    'age': str(row[1]),
                    'experience': str(row[2]),
                    'auto_class_eco': str(row[3]),
                    'january_eco': str(row[4]),
                    'february_eco': str(row[5]),
                    'march_eco': str(row[6]),
                    'april_eco': str(row[7]),
                    'may_eco': str(row[8]),
                    'june_eco': str(row[9]),
                    'july_eco': str(row[10]),
                    'august_eco': str(row[11]),
                    'september_eco': str(row[12]),
                    'october_eco': str(row[13]),
                    'november_eco': str(row[14]),
                    'december_eco': str(row[15]),
                    'auto_class_comfort_plus': str(row[16]),
                    'january_comfort_plus': str(row[17]),
                    'february_comfort_plus': str(row[18]),
                    'march_comfort_plus': str(row[19]),
                    'april_comfort_plus': str(row[20]),
                    'may_comfort_plus': str(row[21]),
                    'june_comfort_plus': str(row[22]),
                    'july_comfort_plus': str(row[23]),
                    'august_comfort_plus': str(row[24]),
                    'september_comfort_plus': str(row[25]),
                    'october_comfort_plus': str(row[26]),
                    'november_comfort_plus': str(row[27]),
                    'december_comfort_plus': str(row[28]),
                    'auto_class_business': str(row[29]),
                    'january_business': str(row[30]),
                    'february_business': str(row[31]),
                    'march_business': str(row[32]),
                    'april_business': str(row[33]),
                    'may_business': str(row[34]),
                    'june_business': str(row[35]),
                    'july_business': str(row[36]),
                    'august_business': str(row[37]),
                    'september_business': str(row[38]),
                    'october_business': str(row[39]),
                    'november_business': str(row[40]),
                    'december_business': str(row[41])
                }

            collection.insert_one(post)  # insert post into collection
