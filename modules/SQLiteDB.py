import sqlite3
import openpyxl


class SQLiteDB:
    """Working with SQLite database"""

    def main(self):
        """Program entry point"""

        dbname = input("Input your database name: ")

        # Create new database.
        conn = sqlite3.connect(dbname + ".db")

        mycursor = conn.cursor()
        mycursor.execute('''CREATE TABLE cars (client, age, experience, auto_class_eco, january_eco, february_eco, 
        march_eco, april_eco, may_eco, june_eco, july_eco, august_eco, september_eco, october_eco, november_eco, 
        december_eco, auto_class_comfort_plus, january_comfort_plus INT, february_comfort_plus INT, 
        march_comfort_plus INT, april_comfort_plus, may_comfort_plus, june_comfort_plus, july_comfort_plus, 
        august_comfort_plus, september_comfort_plus, october_comfort_plus, november_comfort_plus, 
        december_comfort_plus, auto_class_business, january_business, february_business, march_business, 
        april_business, may_business, june_business, july_business, august_business, september_business, 
        october_business, november_business, december_business)''')

        workbook = openpyxl.load_workbook("TotalWB.xlsx")  # output file workbook
        sheet = workbook.active  # specify workbook sheet

        # Transfer data to database.
        for rows in range(2, 10151):
            for row in sheet.iter_rows(min_row=rows, max_row=rows, max_col=42, values_only=True):
                values = [str(row[0]), str(row[1]), str(row[2]), str(row[3]), str(row[4]), str(row[5]), str(row[6]),
                          str(row[7]), str(row[8]), str(row[9]), str(row[10]), str(row[11]), str(row[12]), str(row[13]),
                          str(row[14]), str(row[15]), str(row[16]), str(row[17]), str(row[18]), str(row[19]),
                          str(row[20]), str(row[21]), str(row[22]), str(row[23]), str(row[24]), str(row[25]),
                          str(row[26]), str(row[27]), str(row[28]), str(row[29]), str(row[30]), str(row[31]),
                          str(row[32]), str(row[33]), str(row[34]), str(row[35]), str(row[36]), str(row[37]),
                          str(row[38]), str(row[39]), str(row[40]), str(row[41])]
                query = '''INSERT INTO cars VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
                ?,?,?,?,?,?,?,?,?,?)'''

                mycursor.execute(query, values)  # execute query
                conn.commit()  # commit execution
